console.log(`
================================
||    Desenvolvido por:       ||
||    Agência Vinci           ||  
================================

`)
//slugfy
if (!String.prototype.slugify) {
    String.prototype.slugify = function() {
        return this.toString().toLowerCase()
            .replace(/[àÀáÁâÂãäÄÅåª]+/g, 'a') // Special Characters #1
            .replace(/[èÈéÉêÊëË]+/g, 'e') // Special Characters #2
            .replace(/[ìÌíÍîÎïÏ]+/g, 'i') // Special Characters #3
            .replace(/[òÒóÓôÔõÕöÖº]+/g, 'o') // Special Characters #4
            .replace(/[ùÙúÚûÛüÜ]+/g, 'u') // Special Characters #5
            .replace(/[ýÝÿŸ]+/g, 'y') // Special Characters #6
            .replace(/[ñÑ]+/g, 'n') // Special Characters #7
            .replace(/[çÇ]+/g, 'c') // Special Characters #8
            .replace(/[ß]+/g, 'ss') // Special Characters #9
            .replace(/[Ææ]+/g, 'ae') // Special Characters #10
            .replace(/[Øøœ]+/g, 'oe') // Special Characters #11
            .replace(/[%]+/g, 'pct') // Special Characters #12
            .replace(/\s+/g, '-') // Replace spaces with -
            .replace(/[^\w\-]+/g, '') // Remove all non-word chars
            .replace(/\-\-+/g, '-') // Replace multiple - with single -
            .replace(/^-+/, '') // Trim - from start of text
            .replace(/-+$/, ''); // Trim - from end of text
    };
}


(function() {

    //settings api
    const settings = {
        "url": "/api/catalog_system/pub/category/tree/3/",
        "method": "GET",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json"
        },
    };

    //contrucao menu
    $.ajax(settings).done(function(response) {
        //menu mobile
        if (large < 992) {

            for (response of response) {

                loadMenuMobile(response)

            }

            initMobile();
            $('.main-menu>ul').prepend("<li class='menu-item'><a href='/account'><p>Login | Cadastro </p></a></li>")
        }

        //menu desktop
        if (large >= 992) {

            for (response of response) {
                if (response.Title != 'Todas as Categorias') {
                    loadMenu(response)
                    loadMoreMenu(response)
                } else {
                    loadMoreMenu(response)
                }
            }

        }


    })

    function loadMenuMobile(menu) {
        const menuElement = document.querySelector('.menu-reference');
        let child = menu.children;
        const subUlElement = document.createElement('ul')
        const ulElement = document.createElement('ul');
        const liElement = document.createElement('li');
        const linkElement = document.createElement('a');
        const pElement = document.createElement('p');
        const maxElement = document.createElement('div');
        linkElement.setAttribute('href', menu.url);
        pElement.textContent = menu.Title;
        liElement.setAttribute('class', 'menu-item');
        linkElement.setAttribute('class', 'link-main-menu')
        maxElement.setAttribute('class', 'sub-menu');
        linkElement.appendChild(pElement);
        liElement.appendChild(linkElement);
        menuElement.before(liElement);
        maxElement.appendChild(ulElement);

        
        for (child of child) {
            const subLiElement = document.createElement('li');
            const subLinkElement = document.createElement('a');

            subLiElement.setAttribute('class', 'sub-menu-all-item');
            subLinkElement.setAttribute('href', child.url);
            subUlElement.setAttribute('class', 'sub-menu-all-item-container');
            subLinkElement.textContent = child.Title;

            subLiElement.appendChild(subLinkElement);
            subUlElement.appendChild(subLiElement);
            liElement.appendChild(subUlElement);
        }
    }

    //LOAD MENU
    function loadMenu(menu) {
        const menuElement = document.querySelector('.menu-reference');
        let child = menu.children;
        const ulElement = document.createElement('ul');
        const liElement = document.createElement('li');
        const linkElement = document.createElement('a');
        const imgElement = document.createElement('img');
        const pElement = document.createElement('p');
        const maxElement = document.createElement('div');
        const imgContainer = document.createElement('div');
        imgContainer.setAttribute('class','img-container');
        linkElement.setAttribute('href', menu.url);
        pElement.textContent = menu.Title;
        liElement.setAttribute('class', 'menu-item');
        linkElement.setAttribute('class', 'link-main-menu')
        maxElement.setAttribute('class', 'sub-menu');
        imgElement.setAttribute('src','/arquivos/icon-menu-'+menu.name.slugify()+'.png');
        imgContainer.appendChild(imgElement)
        linkElement.appendChild(imgContainer);
        linkElement.appendChild(pElement);
        liElement.appendChild(linkElement);
        menuElement.before(liElement);
        maxElement.appendChild(ulElement)
    }

    //LOAD MORE MENU
    function loadMoreMenu(menu) {
       
        const menuElement = document.querySelector('.allmenu .max-container');

        let child = menu.children;
        const liElement = document.createElement('li');
        const linkElement = document.createElement('a');
        const subUlElement = document.createElement('ul')

        liElement.setAttribute('class', 'all-item');
        linkElement.setAttribute('href', menu.url);

        linkElement.textContent = menu.Title;
        liElement.appendChild(linkElement);
        menuElement.appendChild(liElement);

        for (child of child) {
            const subLiElement = document.createElement('li');
            const subLinkElement = document.createElement('a');

            subLiElement.setAttribute('class', 'sub-menu-all-item');
            subLinkElement.setAttribute('href', child.url);
            subUlElement.setAttribute('class', 'sub-menu-all-item-container');
            subLinkElement.textContent = child.Title;

            subLiElement.appendChild(subLinkElement);
            subUlElement.appendChild(subLiElement);
            liElement.appendChild(subUlElement);
        }
    }

})();





//largura da minha tela
const large = window.innerWidth

//CARRINHO
$('.toggler-cart').click(function(e) {
    e.preventDefault()
    $('.carrinho').slideToggle();
})





function initMobile() {


    $('.sub-menu-all-item-container').before("<span class='open-sub'></span>")

    $('.main-head > .max-container').prepend(`
        <span class="toggle-menu">
            <span class="line line-1"></span>
            <span class="line line-2"></span>
            <span class="line line-3"></span>
        </span>
   `);

    $('.toggle-menu').click(function() {
        $(this).toggleClass('active');
        $('.main-menu').toggleClass('active');
        $('.box-shadow').fadeToggle();
        $('body').toggleClass('menu-open')
    })

    $('.open-sub').click(function() {
        $(this).siblings('.sub-menu-all-item-container').slideToggle();
    });

}

setTimeout(() => {
    $('.ui-autocomplete').css({
        "max-width": $('.search-container .busca').width()
    })
}, 1000);

