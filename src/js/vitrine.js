
//functions
function comprarDaVitrine(e){
    e.preventDefault();

    const qtd = e.path[3].querySelector('.shelf-qtd').value;
    const skuId = e.path[3].querySelector('.box-add-to-shelf').getAttribute('item-id');
    const shelfImageUrl = e.path[3].querySelector('.product-image img').getAttribute('src');
    const productUrl = e.path[3].querySelector('.product-image').getAttribute('href');
    const productName = e.path[3].querySelector('.product-name a').textContent;
    const productPrice = e.path[3].querySelector('.best-price').textContent;

    const item = {
        id: skuId,
        quantity: qtd,
        seller: '1'
    };

    vtexjs.checkout.addToCart([item], null, 1)
    .done(function() {
        showModal();
        mostrarItemNoCarrinho(qtd,shelfImageUrl,productUrl,productName,productPrice);
        showTotais();
    });
}

function toMoney(e) {
    let text = e
    text = parseInt(text)
    text = text.toFixed(2)
    text = text * 0.010

    text = text.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
    return text
}

function addQtd(){
    this.previousElementSibling.value = parseFloat(this.previousElementSibling.value) + 1
}

function removeQtd(){
    this.nextElementSibling.value =  parseFloat(this.nextElementSibling.value) - 1 > 0 ?  parseFloat(this.nextElementSibling.value) - 1 : 1;
}

function showModal(){
    document.querySelector('.modal-shadow').classList.add('active');
}

function closeModal() { 
    document.querySelector('.modal-shadow').classList.remove('active');
}

function mostrarItemNoCarrinho(cartQtd,cartImageUrl,cartUrl,cartName,cartPrice) {
    document.querySelector('.mini-cart-qty-admake').textContent = parseFloat(document.querySelector('.mini-cart-qty-admake').textContent) + 1
    document.querySelector('.mini-cart-itens').innerHTML +=
    `<div class="mini-cart-item">		
        <span class="imagem">		
            <a class="sku-imagem" href="${cartUrl}">
                <img src="${cartImageUrl}"/>
            </a>
        </span>	
        <span class="detalhes">
            <p class="nome-produto">
                <a href="${cartUrl}">${cartName}</a>
            </p>		
            <span class="qtd-valor">			
                <span class="qtd">${cartQtd}x</span>
                <span class="preco">${cartPrice}</span>
            </span>	
        </span>
    </div>
    `;
}

function showTotais() {
    vtexjs.checkout.getOrderForm()
    .done(function(orderForm) {
        document.querySelector('#mini-cart-admake-total').textContent = toMoney(orderForm.value);
    });
}

window.onload = function () {
    //listners
    document.querySelectorAll('.box-item .add a.btn-add-buy-button-asynchronous').forEach(function(item){
        item.addEventListener('click',comprarDaVitrine,false)
    });
    document.querySelectorAll('.box-item .shelf-btn-mais').forEach(function(item){
        item.addEventListener('click',addQtd,false)
    });
    document.querySelectorAll('.box-item .shelf-btn-menos').forEach(function(item){
        item.addEventListener('click', removeQtd,false)
    });
}


