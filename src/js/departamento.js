$(document).ready(function() {
    $('.bread-crumb ul li').first().html('<a href="/">Início</a>');
});


setTimeout(() => {
    $('.search-single-navigator a').each(function() {
        let text = $(this).text();
        text = text.split('(', 1)[0];
        $(this).text(text);
    })
}, 1000)



const width = $(document).width();
if (width < 992) {
    $('.department-link').prepend("<span class='toggler-busca'>Refinar busca</span>");

    $('.toggler-busca').click(function() {
        $('.department-link').toggleClass('active')
    })
}