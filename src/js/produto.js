window.onload = function(){
    const productEspec = document.querySelector('.especificacao-p #caracteristicas')

    if(!productEspec.innerHTML){
        document.querySelector('.especificacao-p').remove()
    }

    const productDescrip = document.querySelector('.descricao-p .productDescription')

    if(!productDescrip.innerHTML){
        document.querySelector('.descricao-p').remove()
    }
}