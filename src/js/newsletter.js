let htmlSucess = "";
let htmlError = "";
let htmlOk = "";

htmlSucess += '<div class="news-sucess">';
htmlSucess += '<div class="news-container news-sucess-txt">';
htmlSucess += '<p>Seu cadastro foi efetuado com sucesso, Obrigado!</p>';
htmlSucess += '</div>';
htmlSucess += "</div";

htmlError += '<div class="news-error">';
htmlError += '<div class="news-container news-error-txt">';
htmlError += '<p>Por favor, verifique se seu email é váliido.</p>';
htmlError += '</div>';
htmlError += "</div";

htmlOk += '<div class="news-ok">';
htmlOk += '<div class="news-container news-ok-txt">';
htmlOk += '<p>Olá, seu usuário já foi cadastrado, Obrigado!</p>';
htmlOk += '</div>';
htmlOk += "</div";

$('.news-input').submit(function(e) {
    e.preventDefault()
    const email = $("#news-email").val();

    url = "/api/dataentities/CL/documents/";

    obj = {
        "email": email,
        "isNewsletterOptIn": true
    }
    $.ajax({
        headers: {
            'Accept': 'application/vnd.vtex.ds.v10+json',
            'Content-Type': 'application/json'
        },

        data: JSON.stringify(obj),
        type: 'PATCH',
        url: url,

        success: function(data) {
            if (data == undefined) {
                $('body').append(htmlOk);

                setTimeout(() => {
                    removePopUp('.news-ok');
                }, 2000);
            } else {
                $('body').append(htmlSucess);

                setTimeout(() => {
                    removePopUp('.news-sucess');
                }, 2000);
            }

            $("#news-email").val('');
        },

        error: function(data) {

            $('body').append(htmlError);
            console.log(htmlError)
            setTimeout(() => {
                removePopUp('.news-error');
            }, 2000);
            $("#news-email").val('');
        }
    });
});


function removePopUp(pop) {
    document.querySelector(pop).remove();
    $(".news-email").val('');
}